package com.example.test.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@RefreshScope
@EnableSwagger2
public class TestController {
	
	@Value("${name}")
	private String port;
	
	@Value("${add}")
	private String add;
	
	@Value("${spring.boot.admin.client.url}")
	private String url;
	
	@GetMapping("/health")
	public void health() {
		System.out.println(port+" "+add+" "+url);
	}

}
